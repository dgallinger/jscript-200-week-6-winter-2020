$(document).ready(function () {
/**
 * Toggles "done" class on <li> element
 */
$('li').on('click', function() {
  $(this).attr('class', 'done');
 });

/**
 * Delete element when delete link clicked
 */
$('.delete').on('click', function() {
  $(this).parent().remove();
});

/**
 * Adds new list item to <ul>
 */
const addListItem = function(e) {
  //e.preventDefault();
  const text = $('input').val();

  // rest here...
  //span element
  const $span = $('<span>');
  $span.text(text);


  //delete element
  const $delete = $('<a>');
  $delete.text('Delete');
  $delete.attr('class', 'delete');

  //create list element
  const $li = $('<li>');
  $li.append($span);
  $li.append($delete);

  $('ul').append($li);

  //add listeners
  $('li').on('click', function() {
    $(this).attr('class', 'done');
   });

  $('.delete').on('click', function() {
    $(this).parent().remove();
  });

  //clear the text box
  $('input').val('');

};

// add listener for add
$('.add-item').on('click', function() {
  addListItem();
});
});